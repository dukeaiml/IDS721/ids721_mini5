DROP TABLE IF EXISTS my_table;

CREATE TABLE IF NOT EXISTS my_table (
    id SERIAL PRIMARY KEY,
    Education VARCHAR(255),
    Experience VARCHAR(255),
    Location VARCHAR(255),
    Job_Title VARCHAR(255),
    Age INT,
    Gender VARCHAR(255),
    Salary DOUBLE PRECISION
);

LOAD DATA LOCAL INFILE './salary_prediction_data.csv'
INTO TABLE my_table
FIELDS TERMINATED BY ','
ENCLOSED BY '"'
LINES TERMINATED BY '\n'
IGNORE 1 ROWS
(Education, Experience, Location, Job_Title, Age, Gender, Salary);

SELECT COUNT(*) FROM my_table;