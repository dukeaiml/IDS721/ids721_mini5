use lambda_http::{run, service_fn, Request, Response, Body, Error};
use mysql_async::{Pool, prelude::Queryable};




async fn function_handler(_event: Request) -> Result<Response<Body>, Error> {
    // 构建数据库连接URL
    tracing::info!("Rust function invoked");
    let database_url = "mysql://admin:Lmt123456@myrustdb.cex915ik2op0.us-east-1.rds.amazonaws.com:3306/myrustdb";

    // 创建数据库连接池
    let pool = Pool::new(database_url);

    let mut conn = pool.get_conn().await.expect("Failed to connect to database");

    let row: Option<(i32,)> = conn.query_first("SELECT COUNT(*) FROM my_table").await.expect("Failed to execute query");

    // 构建响应
    let res_body = match row {
        Some((result,)) => format!("Query result: {}", result),
        None => "No result found".to_string(),
    };

    let response = Response::builder()
        .status(200)
        .header("Content-Type", "text/plain")
        .body(Body::from(res_body))
        .expect("Failed to build response");
    tracing::info!( "Rust function responds to call");
    Ok(response)
}



#[tokio::main]
async fn main() -> Result<(), Error> {
    let openssl_dir = match std::env::var("OPENSSL_DIR") {
        Ok(val) => val,
        Err(_) => "/opt/homebrew/opt/openssl@3".to_string(), // 设置默认路径
    };

    // 设置 OpenSSL 相关的链接路径
    println!("cargo:rustc-link-search=native={}/lib", openssl_dir);

    // 设置 OpenSSL 相关的包含路径
    println!("cargo:include={}/include", openssl_dir);
    tracing_subscriber::fmt().json()
    .with_max_level(tracing::Level::INFO)
    // this needs to be set to remove duplicated information in the log.
    .with_current_span(false)

    // disabling time is handy because CloudWatch will add the ingestion time.
    .without_time()
    // remove the name of the function from every log entry
    .with_target(false)
    .init();
    // Start the Lambda function
    run(service_fn(function_handler)).await
}
